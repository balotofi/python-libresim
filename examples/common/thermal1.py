"""
This example demonstrates the manual creation of thermal network models.

"""

from libresim import Model, EntryPoint, EntryPointPublisher
from libresim.lib.common.thermal import (
    ThermalNetwork,
    create_hot_object,
    create_thermal_node,
    create_related_hot_object,
)


thermal_network = ThermalNetwork("ThermalNetwork")
heater1 = create_hot_object("Heater1", status=1)
thermistor1 = create_thermal_node(
    "Thermistor1",
    base_temperature=20,
    current_temperature=25,
    rise_rate=2,
    fall_rate=1,
)
thermistor1.add_related_hot_object(
    create_related_hot_object(
        "Thermistor1_Heater1", hot_object_name="Heater1", maximum_effect=20
    )
)
thermal_network.add_hot_object(heater1)
thermal_network.add_thermal_node(thermistor1)


class Example(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_entry_point(
            EntryPoint("print_output", "", self, function=self.print_output)
        )

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.get_entry_point("print_output"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )

    def print_output(self):
        print(
            f"{thermistor1.get_name()}: {thermistor1.current_temperature.get_value()}"
        )
        print()


root = [Example("Example"), thermal_network]
