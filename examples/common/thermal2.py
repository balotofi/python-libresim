"""
This example demonstrates the loading of thermal network from file.

"""

from libresim import Model, EntryPoint, EntryPointPublisher
from libresim.lib.common.thermal import create_thermal_network_from_file


# Load from config file:
thermal_network = create_thermal_network_from_file(
    "config/thermal_config.yaml", delta_time=1
)


class Example(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_entry_point(
            EntryPoint("print_output", "", self, function=self.print_output)
        )

    def connect(self):
        self._scheduler.add_simulation_time_event(
            self.get_entry_point("print_output"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )

    def print_output(self):
        thermal_nodes = self._resolver.resolve_absolute(
            "Models/ThermalNetwork/ThermalNodes"
        )
        for tn in thermal_nodes.get_components():
            print(f"{tn.get_name()}: {tn.current_temperature.get_value()}")
        print()


root = [Example("Example"), thermal_network]
