from libresim.lib.common import create_coordinate_system_tree_from_file


coordinate_system_tree = create_coordinate_system_tree_from_file(
    "config/coordinate_system_config.yaml"
)

root = [coordinate_system_tree]
