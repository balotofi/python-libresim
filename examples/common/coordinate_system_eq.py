from libresim import Model, EntryPoint
from libresim.lib.common.models.coordinate_system import CoordinateSystem
from libresim.lib.common.models.celestial.coordinate_systems import (
    EarthCentricEquatorial,
)


CoordSys = CoordinateSystem("CoordSys", "", None)
CoordSysGeoEq = EarthCentricEquatorial("CoordSysGeoEq", "", None)


class Example(Model):
    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        self.ep_print_fields = EntryPoint(
            "print", "", self, function=self.print_fields
        )

    def connect(self):
        coordsys = self._resolver.resolve_absolute("Models/CoordSysGeoEq")
        self._scheduler.add_simulation_time_event(
            coordsys.get_entry_point("update"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )
        self._scheduler.add_simulation_time_event(
            self.ep_print_fields, simulation_time=0, cycle_time=1, repeat=-1
        )

    def print_fields(self):
        CoordSysGeoEq = self._resolver.resolve_absolute("Models/CoordSysGeoEq")
        print(
            CoordSysGeoEq.get_name(),
            CoordSysGeoEq._time_keeper.get_epoch_time(),
            CoordSysGeoEq.position.x.get_value(),
            CoordSysGeoEq.position.y.get_value(),
            CoordSysGeoEq.position.z.get_value(),
            CoordSysGeoEq.rotation.q1.get_value(),
            CoordSysGeoEq.rotation.q2.get_value(),
            CoordSysGeoEq.rotation.q3.get_value(),
            CoordSysGeoEq.rotation.q4.get_value(),
        )


root = [CoordSys, CoordSysGeoEq, Example("Example", "", None)]
