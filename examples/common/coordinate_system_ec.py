from libresim import Model, EntryPoint
from libresim.lib.common.models.coordinate_system import CoordinateSystem
from libresim.lib.common.models.celestial.coordinate_systems import (
    EarthCentricEcliptical,
)


CoordSys = CoordinateSystem("CoordSys", "", None)
CoordSysGeoEc = EarthCentricEcliptical("CoordSysGeoEc", "", None)


class Example(Model):
    def __init__(self, name, description, parent):
        Model.__init__(self, name, description, parent)
        self.ep_print_fields = EntryPoint(
            "print", "", self, function=self.print_fields
        )

    def connect(self):
        coordsys = self._resolver.resolve_absolute("Models/CoordSysGeoEc")
        self._scheduler.add_simulation_time_event(
<<<<<<< HEAD:examples/coordinate_system_ec.py
            coordsys.get_entry_point("update"), simulation_time=0, cycle_time=1, repeat=-1)
=======
            coordsys.get_entry_point("update"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )
>>>>>>> 203e9e54adfd6f9d9db5199bde5f52eee82d903b:examples/common/coordinate_system_ec.py
        self._scheduler.add_simulation_time_event(
            self.ep_print_fields, simulation_time=0, cycle_time=1, repeat=-1
        )

    def print_fields(self):
        CoordSysGeoEc = self._resolver.resolve_absolute("Models/CoordSysGeoEc")
        print(
            CoordSysGeoEc.get_name(),
            CoordSysGeoEc._time_keeper.get_epoch_time(),
            CoordSysGeoEc.position.x.get_value(),
            CoordSysGeoEc.position.y.get_value(),
            CoordSysGeoEc.position.z.get_value(),
<<<<<<< HEAD:examples/coordinate_system_ec.py
            # CoordSysGeoEc.rotation.q1.get_value(),
            # CoordSysGeoEc.rotation.q2.get_value(),
            # CoordSysGeoEc.rotation.q3.get_value(),
            # CoordSysGeoEc.rotation.q4.get_value(),
            )
=======
            CoordSysGeoEc.rotation.q1.get_value(),
            CoordSysGeoEc.rotation.q2.get_value(),
            CoordSysGeoEc.rotation.q3.get_value(),
            CoordSysGeoEc.rotation.q4.get_value(),
        )
>>>>>>> 203e9e54adfd6f9d9db5199bde5f52eee82d903b:examples/common/coordinate_system_ec.py


root = [CoordSys, CoordSysGeoEc, Example("Example", "", None)]
