import libresim


class Counter(libresim.Model):

    def configure(self):
        self._count = 0
        self.count_entrypoint = libresim.EntryPoint(
            "Increase Counter", "", self, function=self.count)
        self.reset_entrypoint = libresim.EntryPoint(
            "Reset Counter", "", self, function=self.reset)

    def connect(self):
        self.scheduler.add_simulation_time_event(
            self.count_entrypoint, simulation_time=1, cycle_time=1, repeat=-1)
        self.scheduler.add_simulation_time_event(self.reset_entrypoint, 5)
        self.logger.log_debug(self, "Counter Model is now connected")

    def reset(self):
        self._count = 0

    def count(self):
        self._count += 1
        self.logger.log_debug(self, f"count is now {self._count}")

    def add(self, offset):
        self._count += offset
        return self._count


root = Counter("Counter", "A simple counter", None)
