import libresim.generic.models.thermal as libresim
import libresim as libsim

thermal_network = libresim.create_thermal_network(
    "TNET", "MyTNET", None,
    delta_time=1)

hot_objects = libresim.HotObjects("HotObjects", "", thermal_network)
HO_1 = libresim.create_hot_object(
    "HO_1", "", hot_objects,
    status=True)
hot_objects.add_component(HO_1)
thermal_network.add_container(hot_objects)

thermal_nodes = libresim.ThermalNodes("ThermalNodes", "", thermal_network)
TN_1 = libresim.create_thermal_node(
    "TN_1", "", thermal_nodes,
    base_temperature=20,
    current_temperature=25,
    rise_rate=1,
    fall_rate=1,
    offset=0,
    scale=1)
thermal_nodes.add_component(TN_1)
thermal_network.add_container(thermal_nodes)

related_hot_objects = libresim.RelatedHotObjects("RelatedHotObjects", "", TN_1)
TN_1_HO_1 = libresim.create_related_hot_object(
    "TN_1_HO_1", "", related_hot_objects,
    HO_1,
    20)
related_hot_objects.add_component(TN_1_HO_1)
TN_1.add_container(related_hot_objects)


# TODO: load from config file:
# thermal_network = libresim.ThermalNetwork.from_config(
#     "thermal_config.yaml", "TNet", "MyTNet", parent=None, delta_time=1)

# root = thermal_network

#----------------------------------------------------------------------------
class Count_simulate_count(libsim.Model):
    def configure(self):
        self._count = 0
        self.count_entrypoint = libresim.EntryPoint(
            "Increase Counter", "", self, function=self.count)
        self.reset_entrypoint = libresim.EntryPoint(
            "Reset Counter", "", self, function=self.reset)

    def connect(self):
        self.scheduler.add_simulation_time_event(
            self.count_entrypoint, simulation_time=1, cycle_time=1, repeat=-1)
        self.scheduler.add_simulation_time_event(self.reset_entrypoint, 5)
        self.logger.log_event(self, "Thermal Model is now connected")

    def reset(self):
        self._count = 0

    def count(self):
        self._count += 1
        self.logger.log_debug(self, f"count is now {self._count}")
        # if self._count == 5 continue

    def simulate(self):
        get_HO_1_status = libresim.hot_object.HotObject.get_status(HO_1)
        print(get_HO_1_status)

    def add(self, offset):
        self._count += offset
        return self._count

root = Count_simulate_count("Thermal Simulator", "Count to five, get status of hot object, count to five, exit", None)

# count_ = 0

# get_HO_1_status = libresim.hot_object.HotObject.get_status(HO_1)
# print(get_HO_1_status)