from .object import Object


class ReferenceFull(Exception):
    pass


class CannotRemove(Exception):
    pass


class NotReferenced(Exception):
    pass


class Reference(Object):
    def __init__(self, name, description="", parent=None):
        Object.__init__(self, name, description, parent)
        self._components = []
        self._max_count = None
        self._min_count = None

    def get_component(self, name):
        """Query for a referenced component by its name."""
        for component in self._components:
            if component.get_name() == name:
                return component

    def get_components(self):
        """Query for the collection of all referenced components."""
        return self._components

    def add_component(self, component):
        """Add a referenced component."""
        if self._max_count is not None:
            if len(self._components) >= self._max_count:
                raise ReferenceFull()

        self._components.append(component)

    def add_components(self, components):
        for component in components:
            self.add_component(component)

    def remove_component(self, component):
        """Remove a referenced component."""
        if self._min_count is not None:
            if len(self._components) <= self._min_count:
                raise CannotRemove()

        if component not in self._components:
            raise NotReferenced()
        self._components.remove(component)

    def get_count(self):
        """Query for the number of components in the collection."""
        return len(self._components)

    def get_upper(self):
        """Query the maximum number of components in the collection."""
        return self._max_count if self._max_count is not None else -1

    def get_lower(self):
        """Query the minimum number of components in the collection."""
        return self._min_count if self._min_count is not None else -1
