from .object import Object


class ContainerFull(Exception):
    pass


class DuplicateName(Exception):
    pass


class CannotDelete(Exception):
    pass


class NotContained(Exception):
    pass


class Container(Object):
    def __init__(self, name, description="", parent=None):
        Object.__init__(self, name, description, parent)
        self._components = []
        self._max_count = None
        self._min_count = None

    def get_component(self, name):
        """Query for a component contained in the container by name."""
        for component in self._components:
            if component.get_name() == name:
                return component

    def get_components(self):
        """Query for the collection of all components in the container."""
        return self._components

    def add_component(self, component):
        """Add a contained component to the container."""
        if self._max_count is not None:
            if len(self._components) >= self._max_count:
                raise ContainerFull()

        for _component in self._components:
            if _component.get_name() == component.get_name():
                raise DuplicateName()

        component.set_parent(self)
        self._components.append(component)

    def add_components(self, components):
        for component in components:
            self.add_component(component)

    def delete_component(self, component):
        """Delete a contained component from the container, and from memory."""
        if self._min_count is not None:
            if len(self._components) <= self._min_count:
                raise CannotDelete()

        for _component in self._components:
            if _component.get_name() == component.get_name():
                self._components.remove(component)
                return
        else:
            raise NotContained()

    def get_count(self):
        """Query for the number of components in the collection."""
        return len(self._components)

    def get_upper(self):
        """Query the maximum number of components in the collection."""
        return self._max_count if self._max_count is not None else -1

    def get_lower(self):
        """Query the minimum number of components in the collection."""
        return self._min_count if self._min_count is not None else -1
