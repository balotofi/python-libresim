from .component import Component


class EventConsumer(Component):
    def __init__(self, name, description="", parent=None):
        Component.__init__(self, name, description, parent)
        self._event_sinks = []

    def get_event_sinks(self):
        return self._event_sinks

    def get_event_sink(self, name):
        for event_sink in self._event_sinks:
            if event_sink.get_name() == name:
                return event_sink

    def add_event_sink(self, event_sink):
        self._event_sinks.append(event_sink)

    def clear(self):
        self._event_sinks = []
