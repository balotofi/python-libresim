from .component import Component


class Model(Component):
    pass


class FallibleModel(Model):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        self._failures = []

    def is_failed(self):
        for failure in self._failures:
            if failure.is_failed():
                return True
        else:
            return False

    def get_failures(self):
        return self._failures

    def get_failure(self, name):
        for failure in self._failures:
            if failure.get_name() == name:
                return failure
