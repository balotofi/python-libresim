from datetime import datetime

from ..service import Service


class TimeKeeper(Service):
    def __init__(self, name, description="", parent=None):
        Service.__init__(self, name, description, parent)
        self._epoch_time = datetime(2020, 1, 1)
        self._mission_start_time = datetime(2020, 1, 1)
        self._simulation_time = 0

    def set_epoch_time(self, epoch_time):
        self._epoch_time = epoch_time
        event_id = self.get_event_manager().query_event_id(
            "EPOCH_TIME_CHANGED"
        )
        self.get_event_manager().emit(event_id)

    def get_epoch_time(self):
        return self._epoch_time

    def set_mission_start_time(self, mission_start_time):
        self._mission_start_time = mission_start_time
        event_id = self.get_event_manager().query_event_id(
            "MISSION_TIME_CHANGED"
        )
        self.get_event_manager().emit(event_id)

    def get_mission_start_time(self):
        return self._mission_start_time

    def set_mission_time(self, mission_time):
        self._mission_start_time = self._epoch_time - mission_time
        event_id = self.get_event_manager().query_event_id(
            "MISSION_TIME_CHANGED"
        )
        self.get_event_manager().emit(event_id)

    def get_mission_time(self):
        return self._epoch_time - self._mission_start_time

    def get_simulation_time(self):
        return self._simulation_time

    def get_zulu_time(self):
        return datetime.utcnow()
