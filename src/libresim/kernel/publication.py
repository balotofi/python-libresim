from .field import StructureField


class Publication:
    def __init__(self, simulator):
        self._simulator = simulator
        self._fields = {}  # using a dict, in lieu of an ordered set
        self._operations = {}

    def publish_field(self, field):
        self._fields[field] = None

        if isinstance(field, StructureField):
            for field_ in field.get_fields():
                self.publish_field(field_)

    def publish_operation(self, operation, description=""):
        self._operations[(operation, description)] = None

    def get_field(self, component, name):
        """Get the field of given name."""
        for field in self._fields:
            if field.get_parent() == component and field.get_name() == name:
                return field

    def get_fields(self):
        """Returns a collection of all fields that have been published."""
        return self._fields.keys()

    def get_fields_of_component(self, component):
        fields = []
        for field in self._fields:
            if field.get_parent() == component:
                fields.append(field)
        return fields
