from .persist import Persist


class Field(Persist):
    pass


class SimpleField(Field):
    def __init__(self, name, description="", parent=None):
        Field.__init__(self, name, description, parent)
        self._value = None

    def set_value(self, value):
        self._value = value

    def get_value(self):
        return self._value


class ForcibleField(SimpleField):
    def __init__(self, name, description="", parent=None):
        SimpleField.__init__(self, name, description, parent)
        self._forced_value = None

    def force(self, value):
        """Force the field value so that it does not change until unforced."""
        self._forced_value = value

    def unforce(self):
        self._forced_value = None

    def get_value(self):
        if self._forced_value is not None:
            return self._forced_value
        else:
            return self._value


class SimpleArrayField(Field):
    def __init__(self, name, description="", parent=None):
        Field.__init__(self, name, description, parent)
        self._values = None

    def get_size(self):
        return len(self._values)

    def get_value(self, index):
        return self._values[index]

    def set_value(self, index, value):
        self._values[index] = value

    def set_values(self, values):
        self._values = values


class StructureField(Field):
    def __init__(self, name, description="", parent=None):
        Persist.__init__(self, name, description, parent)
        self._fields = []

    def get_fields(self):
        return self._fields

    def get_field(self, name):
        for field in self._fields:
            if field.get_name() == name:
                return field

    def __getattr__(self, name):
        return self.get_field(name)
