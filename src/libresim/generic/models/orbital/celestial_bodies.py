'''
The purpose of this model is to represent a celestial body. The available
celestial bodies are defined by the celestial_body_id tag. Currently
the sun, earth, mars and the Earth moon are supported.
'''

from time import time


# how to get current epoch time
print(time())


class CelestialBody():

    def __init__(self, name, celestial_body_id, mass, radius, mu):
        self.name = name
        self.celestial_body_id = celestial_body_id
        self.mass = mass
        self.radius = radius
        self.mu = mu

    def display_celestial_body(self):
        print("Name:", self.name,  ", ID: ", self.celestial_body_id)
