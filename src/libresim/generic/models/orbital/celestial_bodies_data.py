'''
Data Library of Celestial Bodies
'''

# gravitational constant
from libresim.generic.models.orbital.celestial_bodies import CelestialBody


G_meters = 6.67430e-11       # m**3 / kg / s**2
G = G_meters * 10**-9   # km**3/ kg / s**2

earth = CelestialBody(
    name='Earth',
    celestial_body_id=1,
    mass=5.972e24,
    radius=6378.0,
    mu=5.972e24 * G,
    )

moon = CelestialBody(
    name= 'Moon',
    celestial_body_id= 2,
    mass= 5.972e24,
    radius= 1737.4,
    mu= 5.972e24 * G,
)

mars = CelestialBody(
    name='Mars',
    celestial_body_id=3,
    mass=6.39e23,
    radius=3397.0,
    mu=4.282837362069909E+04,
    )

sun = CelestialBody(
    name='Sun',
    celestial_body_id=4,
    mass=1.989e30,
    mu=1.3271244004193938E+11,
    radius=695510.0,
    )

bodies = [earth, moon, mars, sun]

for body in CelestialBody:
    body['diameter'] = body[self.radius] * 2 #--> why is this giving me an error?
