'''
Orbit propagation instantiation file
The purpose of this model is to define the initial orbit settings
and to provide the current orbit state.
'''
import numpy as np
# import math

# initial_conditions = {
# distance: {
#         value: 1.496*math.pow(10, 11),
#         speed: 0.00
#     },
#     angle: {
#         value: math.PI/6,
#         speed: 1.990986*math.pow(10, -7)
#     }
# }

# current_state = {
#     distance: {
#         value: 0,
#         speed: 0
#     },
#     angle: {
#         value: 0,
#         speed: 0
#     }
# }

# Initial Conditions
X_0 = -2500  # [km]
Y_0 = -5500  # [km]
Z_0 = 3400  # [km]
VX_0 = 7.5  # [km/s]
VY_0 = 0.0  # [km/s]
VZ_0 = 4.0  # [km/s]
state_0 = [X_0, Y_0, Z_0, VX_0, VY_0, VZ_0]

# Time Array
t = np.linspace(0, 6*3600, 200)  # Simulates for a time period of 6 hours [s]


def orbit_propagator():
    print('propagating orbit..')
