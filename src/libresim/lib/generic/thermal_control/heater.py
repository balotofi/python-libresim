from yaml import safe_load, YAMLError

from libresim import Reference, SimpleField, EventSink

from libresim.lib.common.electrical import EventEnum
from libresim.lib.common import GenericModel


def create_heater_models_from_file(
    filename, thermal_network=None, electrical_network=None
):
    with open(filename) as file:
        try:
            config = safe_load(file)
        except YAMLError as error:
            print(error)

    models = []
    for name, attributes in config.items():
        if attributes is None:
            attributes = {}

        description = attributes.get("description", "")
        hot_object_name = attributes.get("hot_object")
        electrical_terminator_name = attributes.get("electrical_terminator")
        heater = Heater(name, description)

        if thermal_network and hot_object_name:
            hot_object = thermal_network.get_hot_object(hot_object_name)
            if hot_object:
                heater.set_hot_object(hot_object)

        if electrical_network and electrical_terminator_name:
            electrical_terminator = electrical_network.get_element(
                electrical_terminator_name
            )
            if electrical_terminator:
                heater.set_electrical_terminator(electrical_terminator)

        models.append(heater)
    return models


class Heater(GenericModel):
    def __init__(self, name, description="", parent=None):
        GenericModel.__init__(self, name, description, parent)

        self.add_reference(Reference("HotObject"))
        self.add_reference(Reference("ElectricalTerminator"))

        self.resistance = SimpleField(
            "resistance", "Resistance of the heater in Ohm", self
        )

    def publish(self):
        self._receiver.publish_field(self.resistance)

    def set_hot_object(self, component):
        if self.get_reference("HotObject").get_count() > 0:
            raise Exception
        self.get_reference("HotObject").add_component(component)

    def get_hot_object(self):
        if self.get_reference("HotObject").get_count() > 0:
            return self.get_reference("HotObject").get_components()[0]
        else:
            return None

    def set_electrical_terminator(self, component):
        if self.get_reference("ElectricalTerminator").get_count() > 0:
            raise Exception
        self.get_reference("ElectricalTerminator").add_component(component)

        event_sink = OnPoweredEventSink("OnPowered", parent=self)
        event_source = component.get_event_source(EventEnum.ON_POWERED)
        event_source.subscribe(event_sink)
        event_sink = OnNotPoweredEventSink("OnNotPowered", parent=self)
        event_source = component.get_event_source(EventEnum.ON_NOT_POWERED)
        event_source.subscribe(event_sink)

    def get_electrical_terminator(self):
        if self.get_reference("ElectricalTerminator").get_count() > 0:
            return self.get_reference("ElectricalTerminator").get_components()[
                0
            ]
        else:
            return None

    def is_powered(self):
        terminator = self.get_electrical_terminator()
        if terminator:
            return terminator.is_powered()


class OnPoweredEventSink(EventSink):
    def notify(self, sender, args=None):
        hot_object = self.get_parent().get_hot_object()
        if hot_object:
            hot_object.status.set_value(1)


class OnNotPoweredEventSink(EventSink):
    def notify(self, sender, args=None):
        hot_object = self.get_parent().get_hot_object()
        if hot_object:
            hot_object.status.set_value(0)
