from libresim import (
    Model,
    SimpleField,
    EventSource,
    EventProvider,
    Aggregate,
    Reference,
)

from .constants import EventEnum


class Element(Model, Aggregate, EventProvider):
    """This is the basic node of the network.
    The input voltage is the highest voltage available from input elements.
    The total load (intrinsic plus output) is only drawn from the element with
    highest voltage.

    """

    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        Aggregate.__init__(self, name, description, parent)
        EventProvider.__init__(self, name, description, parent)

        self.add_event_source(EventSource(EventEnum.ON_OPENED))
        self.add_event_source(EventSource(EventEnum.ON_CLOSED))
        self.add_event_source(EventSource(EventEnum.ON_POWERED))
        self.add_event_source(EventSource(EventEnum.ON_NOT_POWERED))

        self.add_reference(Reference("upper_elements", "", self))
        self.add_reference(Reference("lower_elements", "", self))

        self.low_voltage_threshold = SimpleField(
            "low_voltage_threshold",
            "The minimum voltage necessary to power the element",
            self,
        )
        self.intrinsic_load = SimpleField(
            "intrinsic_load", "Intrinsic load of the element", self
        )
        self.load = SimpleField(
            "load",
            "Sum of intrinsic load of the element and all other elements connected further down",
            self,
        )
        self.voltage = SimpleField(
            "voltage",
            "The voltage currently powering the element. This is the highest voltage available from connected input elements.",
            self,
        )

        self._is_powered = False

    def publish(self):
        self._receiver.publish_field(self.low_voltage_threshold)
        self._receiver.publish_field(self.intrinsic_load)
        self._receiver.publish_field(self.load)
        self._receiver.publish_field(self.voltage)

    def determine_voltage(self):
        value = 0
        upper_elements = self.get_reference("upper_elements")
        for upper_element in upper_elements.get_components():
            value = max(value, upper_element.determine_voltage())

        if value < self.low_voltage_threshold.get_value():
            value = 0
        self.voltage.set_value(value)
        return self.voltage.get_value()

    def determine_load(self):
        if self.voltage.get_value() == 0:
            if self._is_powered:
                self._is_powered = False
                self.get_event_source(EventEnum.ON_NOT_POWERED).emit()
        else:
            if not self._is_powered:
                self._is_powered = True
                self.get_event_source(EventEnum.ON_POWERED).emit()

        value = 0
        lower_elements = self.get_reference("lower_elements")
        for lower_element in lower_elements.get_components():
            value += lower_element.determine_load()

        if value != 0:
            value += self.intrinsic_load.get_value()

        self.load.set_value(value)
        return self.load.get_value()
