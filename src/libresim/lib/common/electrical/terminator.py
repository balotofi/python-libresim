from .constants import EventEnum
from .element import Element


def create_terminator(
    name,
    description="",
    parent=None,
    intrinsic_load=None,
    low_voltage_threshold=None,
):
    if None in [intrinsic_load, low_voltage_threshold]:
        raise ValueError
    terminator = Terminator(name, description, parent)
    terminator.intrinsic_load.set_value(intrinsic_load)
    terminator.low_voltage_threshold.set_value(low_voltage_threshold)
    return terminator


class Terminator(Element):
    def validate(self):
        """Terminator can have no elements below itself."""
        if len(self.get_reference("lower_elements").get_components()) != 0:
            return False
        return True

    def determine_load(self):
        if self.voltage.get_value() == 0:
            if self._is_powered:
                self._is_powered = False
                self.get_event_source(EventEnum.ON_NOT_POWERED).emit()
            self.load.set_value(0)
            return self.load.get_value()
        else:
            if not self._is_powered:
                self._is_powered = True
                self.get_event_source(EventEnum.ON_POWERED).emit()
            self.load.set_value(self.intrinsic_load.get_value())
            return self.load.get_value()

    def is_powered(self):
        return self._is_powered
