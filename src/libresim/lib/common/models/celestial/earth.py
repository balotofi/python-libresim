from turtle import pos
from libresim.lib.common.models import CoordinateSystem
from .celestial_body import CelestialBody
import astropy.units as u
from astropy.units import quantity
from astropy.time import Time
from astropy.coordinates import get_body_barycentric, solar_system_ephemeris as sse


class Earth(CelestialBody):
    def __init__(self, name="Earth", description="", parent=None):
        CelestialBody.__init__(self, name, description, parent)

        self.earth_centric_ecliptic = CoordinateSystem("EarthCentricEcliptic")
        self.earth_centric_earth_equatorial = CoordinateSystem(
            "EarthCentricEarthEquatorial"
        )
        self.earth_centric_earth_fixed = CoordinateSystem(
            "EarthCentricEarthFixed"
        )

        self.get_container("CoordinateSystems").add_component(
            self.earth_centric_ecliptic
        )
        self.get_container("CoordinateSystems").add_component(
            self.earth_centric_earth_equatorial
        )
        self.get_container("CoordinateSystems").add_component(
            self.earth_centric_earth_fixed
        )

    def connect(self):
        css = self.get_simulator().get_service("CoordinateSystemService")
        if not css:
            raise Exception

        css.register(self.earth_centric_ecliptic)
        css.register(self.earth_centric_earth_equatorial)
        css.register(self.earth_centric_earth_equatorial)

        self.earth_centric_earth_equatorial.parent_coordinate_system.set_value(
            "EarthCentricEcliptic"
        )
        self.earth_centric_earth_fixed.parent_coordinate_system.set_value(
            "EarthCentricEarthEquatorial"
        )

    def get_distance_to_sun(self):
        # TODO: 
        # take position vector of ecliptic coord and take norm of it.
        # need to use current epoch time for this
        pass

    def do_update(self):
        # TODO: 
        # for EACH of the THREE coordinate systems 
        # update the position -> (x, y, z), 
        # and rotation -> (q1, q2, q3, q4)
        t = self._time_keeper.get_epoch_time()
        pos_ece = get_body_barycentric(
            time=Time(t, scale='tai'), 
            body='Earth').xyz
        self.earth_centric_ecliptic.set_position(pos_ece[0], pos_ece[1], pos_ece[2])
        x = self.earth_centric_ecliptic.position.get_field("x").get_value(),
        y = self.earth_centric_ecliptic.position.get_field("y").get_value(),
        z = self.earth_centric_ecliptic.position.get_field("z").get_value(),
        print (x, y, z)
        print(type(x))
        print (pos_ece.ndim)
