from libresim import Model, Composite, SimpleField, Container


def create_thermal_node(
    name,
    description="",
    parent=None,
    base_temperature=None,
    current_temperature=None,
    rise_rate=None,
    fall_rate=None,
):
    if None in [base_temperature, current_temperature, rise_rate, fall_rate]:
        raise ValueError
    thermal_node = ThermalNode(name, description, parent)
    thermal_node.base_temperature.set_value(base_temperature)
    thermal_node.current_temperature.set_value(current_temperature)
    thermal_node.rise_rate.set_value(rise_rate)
    thermal_node.fall_rate.set_value(fall_rate)
    return thermal_node


class ThermalNode(Model, Composite):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)

        self.add_container(Container("RelatedHotObjects"))

        self.base_temperature = SimpleField(
            "base_temperature", "Base temperature (in C) of thermal node", self
        )
        self.steady_state_temperature = SimpleField(
            "steady_state_temperature",
            "Steady state temperature (in C) of the thermal node",
            self,
        )
        self.current_temperature = SimpleField(
            "current_temperature",
            "Current temperature (in C) of thermal node",
            self,
        )
        self.rise_rate = SimpleField(
            "rise_rate",
            "Temperature rise rate (in C/s) of the thermal node",
            self,
        )
        self.fall_rate = SimpleField(
            "fall_rate",
            "Temperature fall rate (in C/s) of the thermal node",
            self,
        )

    def publish(self):
        self._receiver.publish_field(self.base_temperature)
        self._receiver.publish_field(self.steady_state_temperature)
        self._receiver.publish_field(self.current_temperature)
        self._receiver.publish_field(self.rise_rate)
        self._receiver.publish_field(self.fall_rate)

    def configure(self):
        self._thermal_network = self.get_parent().get_parent()

    def add_related_hot_object(self, related_hot_object):
        self.get_container("RelatedHotObjects").add_component(
            related_hot_object
        )

    def do_update(self, delta_time):
        maximum_effect = 0
        for rel_ho in self.get_container("RelatedHotObjects").get_components():
            hot_object = self._thermal_network.get_hot_object(
                rel_ho.hot_object_name.get_value()
            )
            maximum_effect += (
                hot_object.status.get_value()
                * rel_ho.maximum_effect.get_value()
            )

        self.steady_state_temperature.set_value(
            self.base_temperature.get_value() + maximum_effect
        )

        if (
            self.current_temperature.get_value()
            < self.steady_state_temperature.get_value()
        ):
            new_temperature = self.current_temperature.get_value() + (
                self.rise_rate.get_value() * delta_time
            )
            if new_temperature > self.steady_state_temperature.get_value():
                new_temperature = self.steady_state_temperature.get_value()
        else:
            new_temperature = self.current_temperature.get_value() - (
                self.fall_rate.get_value() * delta_time
            )
            if new_temperature < self.steady_state_temperature.get_value():
                new_temperature = self.steady_state_temperature.get_value()

        self.current_temperature.set_value(new_temperature)
