from libresim.lib.common.models.coordinate_system import CoordinateSystem
from astropy.time import Time
import astropy.units as u
from astropy.coordinates import (
    GeocentricTrueEcliptic,
    HeliocentricMeanEcliptic,
    GCRS,
    ITRS,
    TETE,
)
from scipy.spatial.transform import Rotation as R
import numpy as np


class SunCentricEcliptical(CoordinateSystem):
    """Origin is the Sun and orientation is the ecliptical inertial system
    of J2000. This is also the root coordinate system.

    """

    def do_update(self):
        # nothing to do here, since it is the root coordinate system
        pass


##############################################################################


def create_earth_centric_ecliptical_cs():
    cs = EarthCentricEcliptical("EarthCentricEcliptical", "", None)
    cs.set_parent_coordinate_system("SunCentricEcliptical")
    return cs


class EarthCentricEcliptical(CoordinateSystem):
    """Origin is the Earth and orientation is the ecliptical inertial system
    of J2000. The XY plane coincides with the ecliptic plane of J2000.
    The X-axis points to the predicted mean Vernal equinox at epoch J2000.
    The Z-axis points towards North and completes a right- handed orthogonal
    coordinate system.

    """

    def do_update(self):
        epoch_time = self._time_keeper.get_epoch_time()
        gtec = HeliocentricMeanEcliptic(
            x=0 << u.m,
            y=0 << u.m,
            z=0 << u.m,  # data
            obstime=Time(epoch_time, scale="tai"),  # frame parameters
            representation_type="cartesian",  # boilerplate
        )
        xyz = (
            gtec.transform_to(GeocentricTrueEcliptic(obstime=gtec.obstime))
            .represent_as("cartesian")
            .xyz.to(u.km)
        )
        self.set_position(xyz[0], xyz[1], xyz[2])

        # we create vectors (1, 0, 0) and (0, 1, 0) in the current system
        # without units Heliocentric<->HCRS transforms cannot function
        # because of an origin shift
        vec1 = GeocentricTrueEcliptic(
            x=1 << u.m,
            y=0 << u.m,
            z=0 << u.m,
            obstime=gtec.obstime,
            representation_type="cartesian",
        )
        vec2 = GeocentricTrueEcliptic(
            x=0 << u.m,
            y=1 << u.m,
            z=0 << u.m,
            obstime=gtec.obstime,
            representation_type="cartesian",
        )

        # we transform them to the root system
        vec1trans = (
            vec1.transform_to(HeliocentricMeanEcliptic(obstime=gtec.obstime))
            .represent_as("cartesian")
            .xyz.to(u.km)
        )
        vec2trans = (
            vec2.transform_to(HeliocentricMeanEcliptic(obstime=gtec.obstime))
            .represent_as("cartesian")
            .xyz.to(u.km)
        )

        # we subtract the position
        posTuple = (
            self.position.get_field("x").get_value(),
            self.position.get_field("y").get_value(),
            self.position.get_field("z").get_value(),
        )
        posArray = [c.to_value(u.km) for c in posTuple] << u.km

        # to remove the effect of the origins
        vec1transub = vec1trans - posArray
        vec2transub = vec2trans - posArray

        # this results in 2 transformed vectors
        # we pass them to Rotation.align_vectors

        estimated_rotation, rmsd = R.align_vectors(
            a=np.array([[1, 0, 0], [0, 1, 0]]),
            b=np.array([vec1transub, vec2transub]),
        )
        q4 = estimated_rotation.as_quat()

        # we call .as_quat() and pass it to .
        # q4 = alignedVecs
        self.set_rotation(q4[0], q4[1], q4[2], q4[3])


##############################################################################


def create_earth_centric_equatorial_cs():
    cs = EarthCentricEquatorial("EarthCentricEquatorial", "", None)
    cs.set_parent_coordinate_system("SunCentricEcliptical")
    return cs


class EarthCentricEquatorial(CoordinateSystem):
    """Origin is the Earth and orientation is the equatorial inertial system
    of J2000. The direction of the coordinate axis are defined such that the
    XY plane coincides with the predicted mean Earth equator plane and the
    X axis points towards the predicted mean vernal equinox at the epoch
    J2000 (noon of January 1st 2000). The Z-axis points towards North and
    completes a right-handed orthogonal coordinate system.
    """

    def do_update(self):
        epoch_time = self._time_keeper.get_epoch_time()
        gteq = HeliocentricMeanEcliptic(
            x=0 << u.m,
            y=0 << u.m,
            z=0 << u.m,  # data
            obstime=Time(epoch_time, scale="tai"),  # frame parameters
            representation_type="cartesian",  # boilerplate
        )
        xyz = (
            gteq.transform_to(TETE(obstime=gteq.obstime))
            .represent_as("cartesian")
            .xyz.to(u.km)
        )
        self.set_position(xyz[0], xyz[1], xyz[2])

        vec1 = TETE(
            x=1 << u.m,
            y=0 << u.m,
            z=0 << u.m,
            obstime=gteq.obstime,
            representation_type="cartesian",
        )
        vec2 = TETE(
            x=0 << u.m,
            y=1 << u.m,
            z=0 << u.m,
            obstime=gteq.obstime,
            representation_type="cartesian",
        )

        vec1trans = (
            vec1.transform_to(HeliocentricMeanEcliptic(obstime=gteq.obstime))
            .represent_as("cartesian")
            .xyz.to(u.km)
        )
        vec2trans = (
            vec2.transform_to(HeliocentricMeanEcliptic(obstime=gteq.obstime))
            .represent_as("cartesian")
            .xyz.to(u.km)
        )

        posTuple = (
            self.position.get_field("x").get_value(),
            self.position.get_field("y").get_value(),
            self.position.get_field("z").get_value(),
        )
        posArray = [c.to_value(u.km) for c in posTuple] << u.km

        vec1transub = vec1trans - posArray
        vec2transub = vec2trans - posArray

        estimated_rotation, rmsd = R.align_vectors(
            a=np.array([[1, 0, 0], [0, 1, 0]]),
            b=np.array([vec1transub, vec2transub]),
        )
        q4 = estimated_rotation.as_quat()

        self.set_rotation(q4[0], q4[1], q4[2], q4[3])


##############################################################################


def create_earth_centric_fixed_cs():
    cs = EarthCentricFixed("EarthCentricFixed", "", None)
    cs.set_parent_coordinate_system("SunCentricEcliptical")
    return cs


class EarthCentricFixed(CoordinateSystem):
    """Origin is the Earth and orientation is fixed with Earth rotation.
    The coordinate axes rotate with the Earth. The Z-axis points towards the
    current Earth North pole taking precession and nutation into account.
    The X-axis points towards the 0-meridian. The Y-axis points East an
    completes a right-handed orthogonal coordinate system.

    """

    def do_update(self):
        epoch_time = self._time_keeper.get_epoch_time()
        gtecef = HeliocentricMeanEcliptic(
            x=0 << u.m,
            y=0 << u.m,
            z=0 << u.m,  # data
            obstime=Time(epoch_time, scale="tai"),  # frame parameters
            representation_type="cartesian",  # boilerplate
        )
        xyz = (
            gtecef.transform_to(ITRS(obstime=gtecef.obstime))
            .represent_as("cartesian")
            .xyz.to(u.km)
        )
        self.set_position(xyz[0], xyz[1], xyz[2])
